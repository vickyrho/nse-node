const express = require('express')
const app = express();
const port = 9000
var request = require("request");
var mysql = require('mysql');

app.get('/values', (req, res) => {
    request("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=IBM&outputsize=compact&apikey=KODRG1JWQEETDZ5Q", function(error, response, body) {
        const obj = JSON.parse(body);
        console.log(obj);
        res.send(obj['Time Series (Daily)']);
    });    
})

var con = mysql.createConnection({
  host: "localhost",
  user: "zeus",
  password: "billgates10",
  database: "nse"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

app.get('/',(req,res) => {
    const data = {
            '2020-10-30' : {
                open: "107.9000",
                high: "111.8000",
                low: "107.7500",
                close: "111.6600",
                'adjusted close': "111.6600",
                volume: "7923882",
                'dividend amount': "0.0000",
                'split coefficient': "1.0"
                },
            '2020-10-29': {
                open: "107.2500",
                high: "109.6400",
                low: "106.5500",
                close: "108.9100",
                'adjusted close': "108.9100",
                volume: "6760241",
                'dividend amount': "0.0000",
                'split coefficient': "1.0"
                },
            '2020-10-28': {
                open: "108.6600",
                high: "109.7300",
                low: "105.9200",
                close: "106.6500",
                'adjusted close': "106.6500",
                volume: "9427321",
                'dividend amount': "0.0000",
                'split coefficient': "1.0"
                }
        };
    const itemsArray = [];
    
    for( var item in data) {
        var arrItem = data[item];
        var keyItem = item;
        const finalItem = {
            companyId: 1,
            intraDate: item,
            open: arrItem.open,
            high: arrItem.high,
            close: arrItem.close,
            low: arrItem.low,
            adjusted_close: arrItem['adjusted close'],
            dividend_amount: arrItem['dividend amount'],
            split_coefficient: arrItem['split coefficient'],
            volume: arrItem.volume
        }
        itemsArray.push(finalItem);
        var queryString = 'insert into tick_values(intraDate, open, high, low, close, adjusted_close, volume, dividend_amount, split_coefficient,companyId) values (?,?,?,?,?,?,?,?,?,?)';
        var arrDb = [finalItem.intraDate, finalItem.open, finalItem.high, finalItem.low, finalItem.close, finalItem.adjusted_close, finalItem.volume, finalItem.dividend_amount, finalItem.split_coefficient, finalItem.companyId];  
        con.query(queryString, arrDb, (err, results, fields) => {
            if (err) {
              return console.error(err.message);
            }
            // get inserted id
            console.log('Todo Id:' + results.insertId);
          });
    }
    res.send(itemsArray);
})

app.get('/db', (req,res) => {
    res.json({
        message:'welcome to db',
        code: 200
    })
})
// KODRG1JWQEETDZ5Q



app.listen(port, () => {
console.log(`Example app listening at http://localhost:${port}`)
})